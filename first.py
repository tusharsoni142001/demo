print("Hello world")
# Open a file in write mode ('w')
with open('example.txt', 'w') as file:
    # Write some text to the file
    file.write('Hello, this is some text.\n')
    file.write('Adding more text to the file.\n')
    file.write('And even more text!\n')

print('Text file created and text added.')
